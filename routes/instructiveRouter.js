const express = require('express');
const bodyParser = require('body-parser');
const moongose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');
const Instructives = require('../models/instructives');

const instructiveRouter = express.Router();

instructiveRouter.use(bodyParser.json());

instructiveRouter.route('/')
.options(cors.corsWithOptions, (req, res) => {
     res.sendStatus(200); 
})
.get(cors.cors, (req,res,next) => {
    Instructives.find(req.query)
    .populate('comments.author')
    .then((instructives) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(instructives);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
    Instructives.create(req.body)
    .then((instructive) => {
        console.log('Instructive init', instructive);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(instructive);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /instructives');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
    Instructives.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
})

instructiveRouter.route('/:instructiveId')
.options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200); 
})
.get(cors.cors, (req,res,next) => {
    Instructives.findById(req.params.instructiveId)
    .populate('comments.author')
    .then((instructive) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(instrutive);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /instructive/'+ req.params.instructiveId);
})
.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
    Instructives.findByIdAndUpdate(req.params.instructiveId, {
        $set: req.body
    }, {
        new:true
    })
    .then((instructive) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(instructive);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
    Instructives.findByIdAndRemove(req.params.instructiveId)
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

//-----------------------------------------------------

instructiveRouter.route('/:instructiveId/comments')
.options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200); 
})
.get(cors.cors, (req,res,next) => {
    Instructives.findById(req.params.instructiveId)
    .populate('comments.author')
    .then((instructive) => {
        if(instructive != null){
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(instructive.comments);  
        }
        else {
            err = new Error ('Instructive' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        }       
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    Instructives.findById(req.params.instructiveId)
    .then((instructive) => {
        if(instructive != null){    
            req.body.author = req.user._id;     
            instructive.comments.push(req.body);
            instructive.save()
            .then((instructive) =>{
                Instructives.findById(instructive._id)
                    .populate('comments.author')
                    .then((instructive) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(instructive);
                    })    
                    //console.log('datos: '+req.user._id);           
            }, (err) => next(err)); 
        }
        else {
            err = new Error ('Instructive' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        }       
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /instructive' + req.params.instructiveId+ '/comments');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin , (req, res, next) => {
    Instructives.findById(req.params.instructiveId)
    .then((instructive) => {
        if(instructive != null){            
            for (var i = (instructive.comments.length -1); i>=0; i--){
                instructive.comments.id(instructive.comments[i]._id).remove();
            }
            instructive.save()
            .then((instructive) =>{
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(instructive);
            }, (err) => next(err)); 
        }
        else {
            err = new Error ('Instructive' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        } 
    }, (err) => next(err))
    .catch((err) => next(err));
})

instructiveRouter.route('/:instructiveId/comments/:commentId')
.options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200); 
})
.get(cors.cors, (req,res,next) => {
    Instructives.findById(req.params.instructiveId)
    .populate('comments.author')
    .then((instructive) => {
        if(instructive != null && instructive.comments.id(req.params.commentId) !=null){
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(instructive.comments.id(req.params.commentId));  
        }
        else if (instructive == null) {
            err = new Error ('Instructive' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        } 
        else {
            err = new Error ('Comment' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        }      
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /instructive/'+ req.params.instructiveId + '/comments/' + req.params.commentsId);
})
.put(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    Instructives.findById(req.params.Id)
    .then((instructive) => {
        var AuId = instructive.comments.id(req.params.commentId).author;
        if (instructive != null && instructive.comments.id(req.params.commentId) != null && AuId.equals(req.user._id) ) { 
            if (req.body.rating) {
                instructive.comments.id(req.params.commentId).rating = req.body.rating;
            }
            if (req.body.comment) {
                instructive.comments.id(req.params.commentId).comment = req.body.comment;                
            }
            instructive.save()
            .then((instructive) => {
                Instructives.findById(instructive._id)
                .populate('comments.author')
                .then((instructive) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(instructive);  
                })                             
            }, (err) => next(err));
        }
        else if (AuId !== req.user._id){
            var err = new Error('You are not authorized to perform this operation!');
            err.status = 403;
            return next(err);
        }
        else if (instructive == null) {
            err = new Error('Instructive ' + req.params.instructiveId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);            
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Instructives.findById(req.params.instructiveId)
    .then((instructive) => {
        var AuId = instructive.comments.id(req.params.commentId).author;
        if(instructive != null && instructive.comments.id(req.params.commentId) !=null && AuId.equals(req.user._id)){        
            instructive.comments.id(req.params.commentId).remove();
            instructive.save()
            .then((instructive) =>{
                Instructives.findById(instructive._id)
                .populate('comments.author')
                .then((instructive) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(instructive);  
                }) 
            }, (err) => next(err)); 
        }
        else if (AuId !== req.user._id){
            var err = new Error('You are not authorized to perform this operation!');
            err.status = 403;
            return next(err);
        }
        else if (instructive == null) {
            err = new Error ('Instructive' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        } 
        else {
            err = new Error ('Comment' + req.params.commentId+'not found');
            err.status = 404;
            return next(err);
        }   
    }, (err) => next(err))
    .catch((err) => next(err));
});

//---------------------------------------------------------------------------
instructiveRouter.route('/:instructiveId/comments')
.options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200); 
})
.get(cors.cors, (req,res,next) => {
    Instructives.findById(req.params.instructiveId)
    .populate('steps.instructive')
    .then((instructive) => {
        if(instructive != null){
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(instructive.steps);  
        }
        else {
            err = new Error ('Instructive' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        }       
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    Instructives.findById(req.params.instructiveId)
    .then((instructive) => {
        if(instructive != null){    
            req.body.instructive = req.instructive._id;     
            instructive.steps.push(req.body);
            instructive.save()
            .then((instructive) =>{
                Instructives.findById(instructive._id)
                    .populate('steps.instructive')
                    .then((instructive) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(instructive);
                    })    
                    //console.log('datos: '+req.user._id);           
            }, (err) => next(err)); 
        }
        else {
            err = new Error ('Instructive' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        }       
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /instructive' + req.params.instructiveId+ '/comments');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin , (req, res, next) => {
    Instructives.findById(req.params.instructiveId)
    .then((instructive) => {
        if(instructive != null){            
            for (var i = (instructive.steps.length -1); i>=0; i--){
                instructive.steps.id(instructive.steps[i]._id).remove();
            }
            instructive.save()
            .then((instructive) =>{
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(instructive);
            }, (err) => next(err)); 
        }
        else {
            err = new Error ('Instructive' + req.params.instructiveId+'not found');
            err.status = 404;
            return next(err);
        } 
    }, (err) => next(err))
    .catch((err) => next(err));
})


module.exports = instructiveRouter;