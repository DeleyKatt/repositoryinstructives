const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

const commentSchema = new Schema({
    rating:  {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment:  {
        type: String,
        required: true
    },
    author:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});
const stepsSchema = new Schema({
    step:  {
        type: Number,
        required: true
    },
    text:  {
        type: String,
        required: true
    },    
    instructive:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'instructiveSchema'
    }
}, {
    timestamps: true
});

const instructiveSchema = new Schema({
    name:{
        type: String,
        required:true,
        unique:true
    },
    description:{
        type:String,
        required:true
    },
    category: {
        type: String,
        required: true
    },
    comments:[commentSchema],
    steps:[stepsSchema]
    },{
        timestamps:true
});


var Instructive = mongoose.model('Intructive', instructiveSchema);

module.exports = Instructive;